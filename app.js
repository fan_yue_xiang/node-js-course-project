'use strict';
let Koa=require('koa');
let bodyParser=require('koa-bodyparser');
let controlller=require('./controllers');

let app=new Koa();

app.use(bodyParser());

app.use(controlller());

let port=4000;
app.listen(port,()=>{
    console.log(`http://localhost:${port}`);
})
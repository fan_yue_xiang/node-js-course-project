let list_fn=async(ctx,next)=>{
    ctx.body='不落窠臼';
}
let add_fn=async(ctx,next)=>{
    ctx.body='不知所措';
}
let edit_fn=async(ctx,next)=>{
    ctx.body='不明所以';
}
let delete_fn=async(ctx,next)=>{
    ctx.body='不染一尘';
}
let details_fn=async(ctx,next)=>{
    ctx.body='不以为然';
}
module.exports={
    '/student/list':['get',list_fn],
    '/student/add':['post',add_fn],
    '/student/edit':['put',edit_fn],
    '/student/delete':['delete',delete_fn],
    '/student/details':['get',details_fn]
}